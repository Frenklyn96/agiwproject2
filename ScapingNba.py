from bs4 import BeautifulSoup
import urllib.request
import csv
from selenium import webdriver
import pandas as pd
import numpy as np
import Dizionario as dz

nbalink='https://www.nba.com/stats/player/202322/?Season=2020-21&SeasonType=Regular%20Season'
with webdriver.Edge(executable_path='edgedriver_win64/msedgedriver.exe') as driver:
        driver.get(nbalink)
        soup = BeautifulSoup(driver.page_source, 'lxml')
        name=soup.find("meta", property="og:title")["content"]
      
        table= soup.findAll('div', attrs={'class': 'nba-stat-table__overflow'})
        # Step 3: Read tables with Pandas read_html()
        dfs = pd.read_html(str(table))
        print(f'Total tables: {len(dfs)}')
        print(dfs[0])
        matrixnba=np.array(dfs[0])
        #Scambio colonne nba per renderle uguali a matrixroto
        matrixnba[:, [16, 5]] = matrixnba[:, [5, 16]]
        matrixnba[:, [14, 6]] = matrixnba[:, [6, 14]]
        matrixnba[:, [17, 7]] = matrixnba[:, [7, 17]]
        matrixnba[:, [19, 8]] = matrixnba[:, [8, 19]]
        matrixnba[:, [20, 9]] = matrixnba[:, [9, 20]]
        matrixnba[:, [21, 10]] = matrixnba[:, [10, 21]]
        matrixnba[:, [18, 11]] = matrixnba[:, [11, 18]]
        matrixnba[:, [12, 16]] = matrixnba[:, [16, 12]]
        matrixnba[:, [13, 14]] = matrixnba[:, [14, 13]]
        matrixnba[:, [14, 17]] = matrixnba[:, [17, 14]]
        matrixnba[:, [19, 15]] = matrixnba[:, [15, 19]]
        matrixnba[:, [16, 19]] = matrixnba[:, [19, 16]]
        matrixnba[:, [17, 20]] = matrixnba[:, [20, 17]]
        matrixnba=np.delete(matrixnba, [21,22,23,24,25], 1)
        matrixnba = pd.DataFrame(matrixnba, columns =['YEAR','TM','G','MIN','PTS','REB','OREB','AST','STL','BLK','PF','TO','FGM','FGA','FG%','3PTM','3PTA','3PT%','FTM','FTA','FT%'])
        matrixnba['TM'] = matrixnba['TM'].map(lambda x: dz.cambioTeam(x))
        matrixnba['YEAR']=matrixnba['YEAR'].map(lambda x: int(x.replace ("-","")))
        print(matrixnba)

        matrixnba.to_csv(path_or_buf="nba.csv", index=False)
        driver.close
