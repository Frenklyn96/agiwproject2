from bs4 import BeautifulSoup
import urllib.request
import csv
from selenium import webdriver
import pandas as pd
import numpy as np
import Dizionario as dz
import os.path


dimensioneMassima=0
#restituisce 1 se è andato a buon fine lo scrap se no 0
def Estrapola (link):
       # link="https://www.nbcsportsedge.com/edge/basketball/nba/player/29574/john-wall/stats"
        try:
               with webdriver.Edge(executable_path='edgedriver_win64/msedgedriver.exe') as driver:
                        driver.get(link)
                        soup = BeautifulSoup(driver.page_source, 'lxml')
                        name=soup.find("meta",property="og:description")["content"]
                        nomeGiocatore=name.split(" - ", 1)[0]
                        #print(nomeGiocatore)
                        if (os.path.isfile("giocatori/"+nomeGiocatore+".csv")):
                                return(0)
                        table= soup.findAll('table', attrs={'class': 'stats-table table-scroll__table career-per-game-stats'})
                        # Step 3: Read tables with Pandas read_html()
                        dfs = pd.read_html(str(table))
                        #print(f'Total tables: {len(dfs)}')
                        print(dfs[0])
                        matrixroto=np.array(dfs[0])
                        matrixroto=np.delete(matrixroto, matrixroto.shape[0]-1,0)
                        matrixroto = pd.DataFrame(matrixroto, columns =['YEAR','TM','G','MIN','PTS','REB','OREB','AST','STL','BLK','PF','TO','FGM','FGA','FG%','3PTM','3PTA','3PT%','FTM','FTA','FT%'])
                        matrixroto['FG%'] = matrixroto['FG%'].map(lambda x: x.rstrip('%'))
                        matrixroto['3PT%'] = matrixroto['3PT%'].map(lambda x: x.rstrip('%'))
                        matrixroto['FT%'] = matrixroto['FT%'].map(lambda x: x.rstrip('%'))
                        matrixroto['TM'] = matrixroto['TM'].map(lambda x: dz.cambioTeam(x))
                        matrixroto['YEAR']=matrixroto['YEAR'].map(lambda x: int(x.replace ("-","")))
                        matrixroto.to_csv(path_or_buf="giocatori/"+nomeGiocatore+".csv", index=False)
                        global dimensioneMassima
                        if(dimensioneMassima<matrixroto.shape[0]):
                                dimensioneMassima=matrixroto.shape[0]
                        driver.close
                        return(1)
        except Exception:
              return(0)

j=1
contogiocatoriscrap=85
while contogiocatoriscrap <100:
        link="https://www.nbcsportsedge.com/edge/basketball/nba/player-news#page="
        with webdriver.Edge(executable_path='edgedriver_win64/msedgedriver.exe') as driver:
                temp=link+str(j)
                driver.get(temp)
                soup = BeautifulSoup(driver.page_source, 'lxml')
                ris=soup.findAll("div",attrs={"class":"player-news-article__profile"})
                for player in ris:
                        playersite=player.find("span",attrs={"class":"player-news-article__profile__name"}).find('a')['href']
                        contogiocatoriscrap=contogiocatoriscrap+Estrapola("https://www.nbcsportsedge.com"+playersite+"/stats")
                j=j+1
print ("Row max",dimensioneMassima)



