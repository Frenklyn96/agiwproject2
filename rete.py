from bs4 import BeautifulSoup
import urllib.request
import csv
from selenium import webdriver
import pandas as pd
import numpy as np
import Dizionario as dz
import os.path
import pandas as pd
import glob
import pickle
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics
from sklearn.model_selection import train_test_split




files = glob.glob("giocatori/*.csv")

df = pd.DataFrame()
j=0
a_file = open("listaGiocatori.pkl", "wb")
lista=[]
for f in files:
    csv = pd.read_csv(f)
    csv.insert(0, "id", j)
    j=j+1
    lista.append(f.rstrip(".csv"))
    df = df.append(csv)
pickle.dump(lista, a_file)
a_file.close()
print(df['id'])

#df=df.set_index('id')
pars=['YEAR', 'TM', 'G','MIN','PTS','REB','OREB','AST','STL', 'BLK','PF','TO', 'FGM', 'FGA', 'FG%', '3PTM', '3PTA', '3PT%', 'FTM', 'FTA', 'FT%']
X=df[pars]
Y=df['id']
model = RandomForestClassifier(random_state=1)
model.fit(X, Y)
train_predictions = model.predict(X)
train_acc=metrics.accuracy_score(Y,train_predictions)
print('Accuratezza train: %s'  % train_acc)
train, test = train_test_split(df)
X_test=test[pars]
Y_test=test['id']
test_predictions = model.predict(X_test)
test_acc=metrics.accuracy_score(Y_test,test_predictions)
print('Accuratezza test: %s'  % test_acc)
